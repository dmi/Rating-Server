#ifndef RATINGSERVER_H
#define RATINGSERVER_H

#include <string>

using namespace std;

class CAccount
{
private:
	int id;							// Primary key in AccountData.rs file
	
	string firstName, secondName, thirdName;		// The first name is supposed to be shown in rating lists; the rest is aliases
	
	float trsv;						// Team Rating System ( =: trs ) Value
	
	int nrOfEvaluatedTrsGames;				// Number of evaluated trs games
	
	bool privateNrOfEvaluatedTrsGames;			// Wheter to show "> 20" instead of the real number of games in public (if > 20)
	
	string description;					// E-mail address, website, player info and other voluntary stuff goes here
	
	string password;					// Account password
	
public:
	CAccount(int paramId, string paramFirstName, string paramSecondName, string paramThirdName,	// Trivial constructor
		 bool paramPrivateNrOfEvaluatedTrsGames, string paramDescription, string paramPassword);
	
	~CAccount();
	
	int getId();
	
	string getFirstName();
	
	void setFirstName(string paramFirstName);
	
	string getSecondName();
	
	void setSecondName(string paramSecondName);
	
	string getThirdName();
	
	void setThirdName(string paramThirdName);
	
	float getTrsv();
	
	int getNrOfEvaluatedTrsGames();
	
	int getPublicNrOfEvaluatedTrsGames();
	
	void setNrOfEvaluatedTrsGames(int paramNrOfEvaluatedTrsGames);
	
	bool getPrivateNrOfEvaluatedTrsGames();
	
	void setPrivateNrOfEvaluatedTrsGames(bool paramPrivateNrOfEvaluatedTrsGames);
	
	string getDescription();
	
	void setDescription(string paramDescription);
	
	void setPassword(string paramPassword);
	
	void incrementNrOfEvaluatedTrsGames();
	
	bool passwordMatches(string paramPassword);
	
	void printDetails();					// Show account details
	
	friend void updateRating(int paramId, float paramTrsv, bool verbose);
	
	friend int saveToFile(const char* paramPathToFileToSaveTo);
};

class CAccountListElement	// Elements of the account list (will be sorted by account-id)
{
public:
	int id;
	
	CAccount* account;
	
	CAccountListElement* prevElement;
	CAccountListElement* nextElement;
	
	CAccountListElement();			// Trivial constructor
};

class CRatingListElement	// Elements of the sorted linked list of (playername,rating-value)-pairs (will be sorted by primary rating value)
{
public:
	int rank;
	
	float trsv;
	
	CAccount* account;
	
	CRatingListElement* prevElement;
	CRatingListElement* nextElement;
	
	CRatingListElement();			// Trivial constructor
};

void setupAccountList();
void setupRatingList();
void insertIntoAccountList(CAccountListElement accountListElement);
void insertIntoRatingList(CRatingListElement ratingListElement);
CAccountListElement* extractAccountListElement(int paramId);
CRatingListElement* extractRatingListElement(int paramId);
CAccount* addAccount(int paramId, string paramFirstName, string paramSecondName, string paramThirdName,
		      bool paramPrivateNrOfEvaluatedTrsGames, string paramDescription, string paramPassword, bool verbose = false);
void removeAccount(int paramID);
int getIdFromName(string paramName);
CAccount* getAccountFromId(int paramId);
CAccount* getAccountFromName(string paramName);
void updateRating(int paramId, float paramTrsv, bool verbose = false);
void printAccountList();
void printRatingList();
int loadFromFile(const char* paramPathToFileToLoadFrom);
int saveToFile(const char* paramPathToFileToSaveTo);

extern int numberOfRegisteredAccounts;
extern int idOfLastRegisteredAccount;

extern CAccountListElement* accountListStart;
extern CAccountListElement* accountListEnd;

extern CRatingListElement* ratingListStart;
extern CRatingListElement* ratingListEnd;

extern const char* pathToFileToSaveTo;

#endif
