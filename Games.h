#ifndef GAMES_H
#define GAMES_H

#include <stdio.h>
#include <string>

#include "RatingServer.h"

using namespace std;

class CPlayerListElement;
class CResultListElement;

class CGameListElement
{
public:
	int gameServerSocketFd;
	char gameKey[4];
	
	CPlayerListElement* playerListStartTeam1;
	CPlayerListElement* playerListStartTeam2;
	
	CResultListElement* resultListStart;
	
	CGameListElement* nextElement;
	
	CGameListElement(int paramGameServerSocketFd, char* paramGameKey);	// Trivial constructor
};

class CPlayerListElement
{
public:
	int playerNr;
	
	char* gameKey[4];
	char* playerKey[4];
	
	CAccount* account;
	string chosenNickname;
	
	float efficiency;
	
	CPlayerListElement* nextPlayer;
};

class CResultListElement : public CGameListElement
{
public:
	int frequency;
	
	char winnerTeam;		// 1,2 or 0 (for a draw)
	
	bool isEqualTo(CResultListElement paramCResultListElement);
	bool isSimilarTo(CResultListElement paramCResultListElement);
};

CGameListElement* addGame(int paramGameServerSocketFd);
void removeGame(CGameListElement* paramGameListElement);
CGameListElement* getGameByGameKey(char* paramGameKey);
CGameListElement* getGameBySocketFd(int paramSocketFd);

extern CGameListElement* gameListRoot;

extern CGameListElement* gameListStart;
extern CGameListElement* gameListEnd;

#endif
